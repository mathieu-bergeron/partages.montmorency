#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

import argparse
import json
import codecs
import re

parser = argparse.ArgumentParser(description='From Forms .csv to .json')
parser.add_argument('-d', metavar='DELIMITER', type=str, default=',', help='Delimiter')
parser.add_argument('-ld', metavar='LIST_DELIMITER', type=str, default=';', help='List delimiter')
parser.add_argument('-i', metavar='CSV', type=str, nargs='+', help='One or more .csv file')
parser.add_argument('-o', metavar='JSON', default='out.json', type=str, help='The output .json file -- defaults to colnet.json')

args = parser.parse_args()

if args.i is None:
    parser.print_usage()
    exit(0)


LIST_DELIMITER = args.ld
DELIMITER = args.d
INPUT_PATHS = args.i
OUTPUT_PATH = args.o

QUOTED_STRING_RE = "\"[^\"]*\"" 
QUOTED_STRING_MATCHER = re.compile(QUOTED_STRING_RE)

seed = 1
def next_quoted_string_placeholder():
    global seed
    placeholder = "___%s___"%seed
    seed += 1
    return placeholder

def parse_list(answer):
    result = None

    segments = answer.split(LIST_DELIMITER)

    if len(segments) == 0:

        result = ""

    elif len(segments) == 1:

        result = segments[0]

    elif len(segments) > 0:

        result = [x for x in segments if len(x) > 0]

    return result

def read_csv_file(input_file, json_out):
    for i, line in enumerate(input_file):
        line = line.rstrip()
        quoted_strings = {}
        matched_quoted_strings = QUOTED_STRING_MATCHER.findall(line)

        for matched_quoted_string in matched_quoted_strings:
            placeholder = next_quoted_string_placeholder()
            matched_quoted_string = matched_quoted_string.lstrip("\"");
            matched_quoted_string = matched_quoted_string.rstrip("\"");
            quoted_strings[placeholder] = matched_quoted_string
            line = line.replace("\"" + matched_quoted_string + "\"", placeholder)

        segments = line.split(DELIMITER)

        json_out[i] = []
        for segment in segments:
            if segment in quoted_strings:
                json_out[i].append(parse_list(quoted_strings[segment]))
            else:
                json_out[i].append(parse_list(segment))



def read_csv(input_path, json_out):
    with codecs.open(input_path, encoding='utf-8') as input_file:
        read_csv_file(input_file, json_out)

def write_json_file(output_file, json_out):
    output_file.write(json.dumps(json_out, sort_keys=True, indent=4, ensure_ascii=False))

def write_json(output_path, json_out):
    with codecs.open(output_path, 'w', encoding='utf-8') as output_file:
        write_json_file(output_file, json_out)


if __name__ == '__main__':

    json_out = {}

    for input_path in INPUT_PATHS:

        read_csv(input_path, json_out)

        write_json(OUTPUT_PATH, json_out)

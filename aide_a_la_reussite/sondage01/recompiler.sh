if [ "$1" = "" ]; then
    echo usage $0 IN.CSV
    exit 0
fi

input_file=$1
json_file=$(echo $input_file | sed "s/.csv/.json/")

sed "s/Difficulté de lecture/Difficultés de lecture/g" -i $input_file

./csv_to_json.py -i $input_file -o $json_file

./graphiques.py -i $json_file





#! /usr/bin/python3
# vim: set fileencoding=utf-8 :
#
# --
# Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
# --
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU AFFERO General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
# or see http://www.gnu.org/licenses/agpl.txt.
# --

import argparse
import json
import codecs

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors

parser = argparse.ArgumentParser(description='Compile graphs')
parser.add_argument('-i', metavar='JSON', type=str, help='The input json file')

args = parser.parse_args()

if args.i is None:
    parser.print_usage()
    exit(0)


INPUT_PATH = args.i

INDEX_DES_CATEGORIES = "0"

INDEX_DEFINITIONS = 7
INDEX_DEFINITION_LIBRE = 10
INDEX_ENJEU = 13
INDEX_ENJEU_LIBRE = 16
INDEX_PROJET = 19
INDEX_PROJET_LIBRE = 22

couleurs = ["tab:red","tab:blue","tab:orange","tab:pink","tab:green","tab:gray"]
couleur_courante = 0;
def prochaine_couleur():
    global couleur_courante
    resultat = couleurs[couleur_courante]
    couleur_courante += 1
    return resultat

def reset_prochaine_couleur():
    global couleur_courante
    couleur_courante = 0

def creer_historgramme(resultats, titre, nom_fichier):

    items_comptes = [(key, resultats[key]) for key in resultats]
    items_comptes.sort(key=lambda x: x[1])
    #items_comptes.reverse()

    items = [x[0] for x in items_comptes]
    comptes = [x[1] for x in items_comptes]
    couleurs = [prochaine_couleur() for x in items_comptes]
    couleurs.reverse()
    reset_prochaine_couleur()


    fig, ax = plt.subplots()

    ax.barh(items, comptes, color=couleurs)
    #ax.set_xticklabels(items, rotation=90, ha='right')

    plt.title(titre)
    #plt.tight_layout()
    plt.savefig(nom_fichier, bbox_inches='tight')


def compter_resultat(resultats, resultat):
    if resultat in resultats:
        resultats[resultat] += 1
    else:
        resultats[resultat] = 1

def creer_graphiques(json_in):
    definitions = {}
    definitions_libres = []
    enjeux = {}
    enjeux_libres = []
    projets = {}
    projets_libres = []

    for index_reponse in json_in:
        if index_reponse != INDEX_DES_CATEGORIES:
            reponses = json_in[index_reponse]
            liste_definitions = reponses[INDEX_DEFINITIONS]
            definition_libre = reponses[INDEX_DEFINITION_LIBRE]
            enjeu = reponses[INDEX_ENJEU]
            enjeu_libre = reponses[INDEX_ENJEU_LIBRE]
            projet = reponses[INDEX_PROJET]
            projet_libre = reponses[INDEX_PROJET_LIBRE]

            for definition in liste_definitions:
                compter_resultat(definitions, definition)

            if len(definition_libre) > 0:
                definitions_libres.append(definition_libre)

            compter_resultat(enjeux, enjeu)

            if len(enjeu_libre) > 0:
                enjeux_libres.append(enjeu_libre)

            compter_resultat(projets, projet)

            if len(projet_libre) > 0:
                projets_libres.append(projet_libre)


    print("## Définitions")
    #print(definitions)
    creer_historgramme(definitions, "Définitions", "definitions.png")
    print("\n".join(definitions_libres))
    print("")

    print("## Enjeux")
    #print(enjeux)
    creer_historgramme(enjeux, "Enjeux", "enjeux.png")
    print("\n".join(enjeux_libres))
    print("")

    print("# Projets")
    #print(projets)
    creer_historgramme(projets, "Projets", "projets.png")
    print("\n".join(projets_libres))
    print("")











if __name__ == "__main__":
    with open(INPUT_PATH) as input_file:
        json_in = json.load(input_file)
        creer_graphiques(json_in)


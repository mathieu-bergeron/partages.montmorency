# Sondage à envoyer au département

## Vision de la réussite

* Avoir une bonne cote R

* Avoir une bonne cote B (bonheur, bien-être)

* Avoir 60% ou plus

* Acquérir les compétences de base

* Un bas taux d'échecs pour un cours

* Prendre sa place comme citoyen/citoyenne

* Avoir un bon taux de diplomation

* Vaincre le vertige du «je ne comprends rien» et avoir le bon état d'esprit pour apprendre

* Des étudiant.es et des enseignant.es engagé.es et motivé.es

## Enjeux qui touchent nos étudiant.es

1. Organisation / assiduité

1. Anxiété

1. Lecture / écriture

1. Formation / dépannage en programmation pour les profs de Sciences de la Nature

## Projets

1. Organisation / assiduité
    * continuer aiguilleur.ca, en particuiler l'aspect suivi Git et/ou cahier de laboratoire

1. Lecture
    * exercices et/ou logiciel relié aux messages d'erreurs (lire un message, trouver les informations clés, trouver des pistes de débogage)

1. Plus spécifique aux cours de programmation
    * exercices de débogage «étape par étape» avec le débogeur d'Eclipse
    * continuer codelescartes.ca, en particulier pour tableaux et débogage (https://ntro.ca/ntro/demo/motivation/)

1. Compétence programmation des profs de Sciences Nat
    * Centre d'aide pour les profs
    * Escouade programmation pour aider les profs "au moment" d'écrire les énoncés

# Autour de la réussite


## Projets

1. Proposer deux projets
    * un projet "messages d'erreur"
    * un projet dépannage



## Attitudes

1. Auto-régulation
    * démarche pour bien travailler
    * p.ex.
        * démarche à suivre en cas d'erreur
        * avant de poser une question, je m'assure de...

    * avec différent niveau d'autonomie

    * avec cahier de laboratoire pour montrer la démarche

    * étudiant doit prouver qu'il a fait la démarche

1. Autonomie
    * pas la même chose que "pas poser de questions"

1. Démarche pour bien travailler
    * lire le message
    * tester avec des hypothèses


## Centre d'aide

1. groupes d'étude ciblés, sur certains contenus
    * cibler des contenus, p.ex. débogage, les boucles
    * cibler des étudiants (sur invitation)

## Modeling


## Nathalie

1. Trouver une façon que ça soit évalué
    * exercices avec différents niveaux


## Nicolas

* traces d'exécution
* méthode de billet à l'entrée
    * pseudo-code de la procédure avant d'entrée
    * ordinograme

* initiative: exemple minimaliste
    * tout petit programme pour tester
    * validateur (pour HTML, dans le cours fonction de travail)

* aider les étudiants pour l'organisation
    * arrimer les examens, les TPs
    * les aider à ne pas faire les travaux à la dernière minute

* évaluer les compétences essentielles
    * grilles d'évaluation pour cibler les compétences
        * (rendu à niveau 1,2,3)
        * renforcer la compétence: répéter, pas acquis

## Ferdaws

1. Cours POO, avec une démo devant la classe
    * focus sur les messages d'erreur
    * modeling, démonstration à voix haute


## Farida

1. Taille et structure des groupes
    * 40 étudiants en théorie (??)
    * groupe 25-30 (??)


## Projet d'aide sur les messages d'erreur

## Dorsaf

1. Tuteur du centre d'aide ou Vincent


## Intéressés

1. Soumaya, Nicolas, Nathalie

## Question à poser

1. Budget pour les dégagements et/ou pour des étudiants??


